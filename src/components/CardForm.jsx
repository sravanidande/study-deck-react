import React from "react";
import { saveCard } from "../services/CardService";

export const CardForm = ({ onSave, onCancel, card }) => {
  const id = card && card.id ? card.id : undefined;
  const [term, setTerm] = React.useState(id ? card.term : "");
  const [definition, setDefinition] = React.useState(id ? card.definition : "");
  const handleTermChange = evt => {
    setTerm(evt.target.value);
  };
  const handleDefinitionChange = evt => {
    setDefinition(evt.target.value);
  };

  const handleReset = () => {
    setTerm("");
    setDefinition("");
    onCancel && typeof onCancel === "function" && onCancel();
  };

  const handleSubmit = evt => {
    evt.preventDefault();
    saveCard({ term, definition, id }).then(card => {
      handleReset();
      onSave && typeof onSave === "function" && onSave(card);
    });
  };
  return (
    <div className="tile">
      <h4>{id ? "Update Card" : "Add Card"}</h4>
      <form onReset={handleReset} onSubmit={handleSubmit}>
        <div>
          <label htmlFor="card_term">term</label>
          <textarea id="card_term" value={term} onChange={handleTermChange} />
        </div>
        <div>
          <label htmlFor="card_definition">Definition</label>
          <textarea
            id="card_definition"
            value={definition}
            onChange={handleDefinitionChange}
          />
        </div>
        <div className="buttons">
          <button className="primary" type="submit">
            save
          </button>
          <button className="secondary" type="reset">
            cancel
          </button>
        </div>
      </form>
    </div>
  );
};
