import React from "react";
import { destroyCard } from "../services/CardService";
import { CardForm } from "./CardForm";

export const CardPreview = ({ onRemove, onUpdate, ...card }) => {
  const [isEditMode, setIsEditMode] = React.useState(false);
  const handleToggleEdit = () => {
    setIsEditMode(!isEditMode);
  };
  return isEditMode ? (
    <CardForm onCancel={handleToggleEdit} card={card} onSave={onUpdate} />
  ) : (
    <View {...card} onEdit={handleToggleEdit} onRemove={onRemove} />
  );
};

export const View = ({ id, term, definition, onRemove, onEdit }) => {
  const [isFront, setIsFront] = React.useState(true);
  const handleCardFlip = () => {
    setIsFront(!isFront);
  };

  const handleRemove = () => {
    const confirm = window.confirm(`Are you sure you want to delete "${term}"`);
    if (confirm) {
      destroyCard(id).then(() => {
        onRemove(id);
      });
    }
  };

  return (
    <div className="tile">
      <h4 className="cardTerm">{isFront ? term : definition}</h4>
      <div className="cardButtons">
        <button type="button" className="tertiary" onClick={handleCardFlip}>
          {isFront ? "show back" : "show front"}
        </button>
        <div>
          <button type="button" className="secondary" onClick={onEdit}>
            edit
          </button>
          <button
            type="button"
            className="secondary danger"
            onClick={handleRemove}
          >
            delete
          </button>
        </div>
      </div>
    </div>
  );
};
