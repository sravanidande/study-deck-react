import React from "react";
import { CardForm } from "./CardForm";
import { CardPreview } from "./CardPreview";
import { Link } from "@reach/router";

export const CardList = ({ cards, onAdd, onRemove, onUpdate }) => {
  return (
    <>
      <h3>Your Cards</h3>
      <div className="practiceCTA">
        <Link to="/practice">Practice</Link>
      </div>
      <div className="gridContainer">
        <CardForm onSave={onAdd} />
        {cards.map(({ id, definition, term }) => (
          <>
            <CardPreview
              key={id}
              id={id}
              definition={definition}
              term={term}
              onRemove={onRemove}
              onUpdate={onUpdate}
            />
          </>
        ))}
      </div>
    </>
  );
};
