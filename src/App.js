import React from "react";
import "./App.css";
import { CardList } from "./components/CardList";
import "./normalize.css";
import { getCard } from "./services/CardService";
import { Router } from "@reach/router";
import { Practice } from "./components/Practice";

function App() {
  const [cards, setCards] = React.useState([]);
  React.useEffect(() => {
    getCard().then(setCards);
  }, []);

  const handleRemove = id => {
    setCards(cards.filter(card => card.id !== id));
  };

  const handleSubmit = card => {
    setCards(cards => [...cards, card]);
  };

  const handleUpdate = card => {
    setCards(cards => cards.map(c => (c.id === card.id ? card : c)));
  };

  return (
    <div>
      <header>
        <h1>
          Study<span className="titleHighlight">Deck</span>
        </h1>
        <h2>Retention through repetition</h2>
      </header>
      <main>
        <Router>
          <CardList
            path="/"
            cards={cards}
            onAdd={handleSubmit}
            onUpdate={handleUpdate}
            onRemove={handleRemove}
          />
          <Practice path="/practice" />
        </Router>
      </main>
    </div>
  );
}

export default App;
